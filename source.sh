#!/bin/bash
# Clone this script in your ROM Repo using following commands.
# cd rom_repo
# curl https://gitlab.com/yu1-tomato/scripts/raw/lineage-15.1/source.sh > source.sh
# to get the device sources simply use bash source.sh
git clone https://gitlab.com/yu1-tomato/device_yu_tomato.git -b lineage-15.1 device/yu/tomato &&  git clone https://github.com/phenomenall/KCUFKernel.git -b o kernel/cyanogen/msm8916 && git clone https://gitlab.com/yu1-tomato/device_cyanogen_msm8916-common.git -b lineage-15.1 device/cyanogen/msm8916-common && git clone https://gitlab.com/yu1-tomato/vendor_yu.git -b lineage-15.1 vendor/yu && git clone https://github.com/LineageOS/android_hardware_cyanogen.git -b lineage-15.0 hardware/cyanogen
